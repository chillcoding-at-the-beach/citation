package com.chillcoding.citation

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import com.chillcoding.citation.model.Citation
import com.chillcoding.citation.model.CitationDb
import com.chillcoding.citation.model.Db
import com.chillcoding.citation.network.CitationService
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class MainActivity : AppCompatActivity(), AnkoLogger {

    companion object {
        const val apiUrl = "http://api.chrisvalleskey.com/"
    }

    lateinit var dialog: Dialog
    val citationDb = CitationDb()
    lateinit var citaList: List<Citation>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dialog = indeterminateProgressDialog(R.string.text_please_wait)
        doAsync {
            citaList = citationDb.requestCitation()
            uiThread {
                showCitation()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.option, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> getCitationFromAPI()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun getCitationFromAPI() {
        val retrofit = Retrofit.Builder()
            .baseUrl(apiUrl)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
        val service = retrofit.create(CitationService::class.java)
        val request = service.listCitation(count = 700)

        dialog.show()
        request.enqueue(object : Callback<Db> {
            override fun onFailure(call: Call<Db>, t: Throwable) {
                dialog.dismiss()
                toast(R.string.text_connection_failed)
            }

            override fun onResponse(call: Call<Db>, response: Response<Db>) {
                val result = response.body()
                citationDb.saveCitation(result!!.db)
                citaList = result.db
                showCitation()
            }
        })
    }

    private fun showCitation() {
        dialog.dismiss()
        var list: MutableList<String> = ArrayList()
        for (c in citaList)
            list.add("${c.quote} -> ${c.source}")
        mainList.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list)
    }
}
