package com.chillcoding.citation.network

import com.chillcoding.citation.model.Db
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CitationService {
    @GET("/fillerama/get.php")
    fun listCitation(@Query("count") count: Int = 20, @Query("format") format: String = "json", @Query("show") show: String = "simpsons"): Call<Db>

}