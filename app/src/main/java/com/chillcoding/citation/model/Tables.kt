package com.chillcoding.citation.model

object CitationTable {
    val NAME = "citation"
    val ID = "_id"
    val source = "source"
    val quote = "quote"
}