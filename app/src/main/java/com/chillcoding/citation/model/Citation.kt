package com.chillcoding.citation.model

data class Citation(var source: String, var quote: String)