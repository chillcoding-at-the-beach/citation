package com.chillcoding.citation.model

import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class CitationDb(private val dbHelper: CitationDbHelper = CitationDbHelper.instance) {

    fun requestCitation() = dbHelper.use {
        select(CitationTable.NAME, CitationTable.source, CitationTable.quote).parseList(classParser<Citation>())
    }

    fun saveCitation(citation: Citation) = dbHelper.use {
        insert(CitationTable.NAME, CitationTable.source to citation.source, CitationTable.quote to citation.quote)
    }

    fun saveCitation(citationList: List<Citation>) = dbHelper.use {
        for (c in citationList)
            saveCitation(c)
    }
}