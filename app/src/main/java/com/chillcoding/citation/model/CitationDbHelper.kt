package com.chillcoding.citation.model

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.chillcoding.citation.App
import org.jetbrains.anko.db.*

class CitationDbHelper(ctxt: Context = App.instance) : ManagedSQLiteOpenHelper(ctxt, DB_NAME, null, DB_VERSION) {

    companion object {
        val instance by lazy { CitationDbHelper() }
        const val DB_NAME = "citation"
        const val DB_VERSION = 2
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(
            CitationTable.NAME, true, CitationTable.ID to INTEGER + PRIMARY_KEY,
            CitationTable.source to TEXT,
            CitationTable.quote to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, v: Int, nv: Int) {
        db.dropTable(CitationTable.NAME)
        onCreate(db)
    }
}